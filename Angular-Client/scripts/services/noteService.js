 'use strict';

 angular.module('Client')

.factory('NoteResource',function($resource)
{

//get the full url
var url = window.location.href

//Then just parse that string

var arr = url.split("/");

//your url is:

var Path = arr[0] + "//" + arr[2]+"/"+arr[3]+"/API/public/";

	return $resource(Path+"notes/",{id:"@id"});

});


